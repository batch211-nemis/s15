console.log ("Hello World");

//JabaScript renders web pages in an interactive and dyanmic fashion. Meaning, it enables us to create dynamically updating content, control multimedia and animate images

//Syntax, Statements and Comments

//Statements:

//Statements in programming are instructions that we tell the computer to perform
//JS statements in programming are instructions that we tell the computer to perform

//JS statements usually end with a semicolon (;)
//Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends

//Syntax in programming, it is the set of rules that describes how statements must be constructed

//Comments are parts of the code that gets ignored by the language
//Comments are meant to described the written code

/* There are two type of comments:
	1. Single line comment(ctrl + /)
	**denoted by two slashes

	2. The multi-line comment (ctrl + shift + /)
	**denoted by a slash and asterisk
*/

//Variables

//It is used to conatin data
//This makes it easier for us to associate information stored in our devices to actual "names" about information

//Declaring variables
//declaring variables tells our devices that a variable name is created and is ready to store data

let myVariable;
console.log(myVariable);

//Declaring a variable without giving it a value will automatically assign it with a value of "undefined meaning the variable's value was not defined."

//Syntax
	//let/const varaibleName;

//console.log() is useful for printing values of variables or certain results of code into Google Chrome browser's console
//constant use of this throughout developing an application will save us time and builds good habits in always checking for the output of our code.

let hello;
console.log (hello); //undefined

//variables must be declared first before they are used
//using variables before they are declared will return an error message

/*
	Guides in wrting variables
		1. Use the 'let' keyword followed by variable name of your choosing and use the assignment operator (=) to assign a value.
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. For contant variables, use the 'const' keyword.
		4. Variable names should be indicative (or descriptive) of teh value being stored to avoid confusion.
*/

//Declare and initialized variables
	//Initializing variables - the instance when a variable is given its initial or starting value
	//Syntax
		//let/const  variableName - value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;
		// const pi = 3.1416;

		//Reassigning variable values
		//reassigning a variable means changing its initial or previous value into another value
		//Syntax
			//variableName = newValue
		productName = 'Laptop';
		console.log(productName);

		// console.log(interest);
		// console.log(pi);

		//let variable cannot be re-declared within its scope so this will work:
		// let friend ='Kate';
		// friend = 'Nej';
		// console.log(friend);

		// let friend ='Kate';
		// let friend = 'Nej'; //eror
		// console.log(friend);
		//error: identfier 'friend' has already been declared

		// interest = 4.489; //error

//Reassigning varaibles sv initializing variables
//declare a avariable first

	let supplier;
	//Initializing is done after the variable has been declared
	//This is considered as initialization because it is the first time that value has been assigned to a variable
	supplier = "John Smith Tradings";
	console.log(supplier);

	//considered as re-assignment because its initial value was already been declared
	supplier = "Zuitt Store"
	console.log(supplier);

	//we cannot declare a const variable without initialization
	// const pi;
	// pi = 3.1416;
	// console.log(pi); //error

	//var 
	//var is also used in declaring a variable
	//let/const they were introduced as new feature in E26 (2015)

	// a = 5;
	// console.log(a); //output:5
	// var a;

	//Hoisting is javascript default behavior of moving declarations to the top
	//In terms of variables and constant, keyword var is hoisted and let and const does not allow hoisting

	// a = 5;
	// console.log(a); //error
	// let a;

//let/const local/global scope

	//Scope essentially means where these variables are available for use
	//let and const are blocked-scoped
	//block is a chunk of code bounded by {}. A block live in curly braces. Anything within curly braces is a block.
	//so a variable declared in a block with let is only available for use within that block

	// let outerVariable = "Hello";
	// {
	// 	let innerVariable = "Hello Again";
	// }
	// console.log(outerVariable);
	// console.log(innerVariable); //innerVariable is not defined

	//Multiple variable declarations
	//Multiple variables may be declared in one line
	//though it is really quicker to do without having to re-type the "let" keyword it is tsill best practice to use multiple "let and const" keywords when declaring varaiable
	//using multiple keywords make it easier to read and determine what kind of variable has been created

	// let productCode = 'DC017', productBrand = "Dell";
	let productCode ='DC017'
	const productBrand = 'Dell';
	console.log(productCode, productBrand);//DC017 Dell

	//Using a variable with a reserved keyword
	// const let = "hello";
	// console.log(let); //error

	//Data Types

	//Strings
	//Strings are series of characters that create a word, phrase, sentence or anything related to creating text
	//Strings in javascript can be written using either a single (') or a double qoute ("")
	//In other programming languages, only the double quotes can be used for creating strings

	let country = 'Philippines';
	let province = "Laguna"

	//Concatenating strings
	//Multiple string values can be combines to create a single string using the "+" symbol.

	let fullAddress = province + ', ' + country;
	console.log(fullAddress); //Laguna, Philippines

	let greeting = "I live in the " + country;
	console.log(greeting); //I live in the Philippines

	//The escape character (\) in strings in combination with other characters can produce various effects
	// "\n" refers to creating a new line in between text

	let mailAddress = 'Metro Manila\n\nPhilippines';
	console.log(mailAddress);
	/*
		Metro Manila

		Philippines
	*/

	//using teh double quotes along with single quotes can allow us to easily include single quotes in texts without using the escape character

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employee went home early';
	console.log(message);

	//Numbers

	//Integers/Whole Numbers
	let headcount = 26;
	console.log (headcount); //26

	//Decimal Numbers/Fractions
	let grade = 98.7;
	console.log(grade);

	//Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance); //2000000000

	//Combining numbers and strings
	console.log("John's grade last quarter is " + grade);
	//John's grade last quarter is 98.7

	//Boolean
	//Boolean values are normally used to store values relating to the state of certain things
	//This will be useful in further discussions about creating logic to make our applictaion respond to certain situations/scenarios

	let isMarried = false;
	let inGoodConduct = true;

	console.log("is Married: " + isMarried);
	console.log("inGoodConduct: " + inGoodConduct);

	//Arrays
	//Array are a special kind of data type that is used to store multiple values
	//Arrays can store different data types but is normally used to store simlar data types

	//similar data types
	//Syntax
		//let/const arrayName = [elementA, elementB, elementC...]
		//Array Literals []
		//Java starts with index zero when counting

		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades);

		//different data types
		//storing different data types inside an array is not recommended because it will NOT make sense in the context of programming

		let details = ["John", "Smith", 32, true];
		console.log(details);

	//Objects
	//Objects are another special kind of data type that's used to mimic real world objects/items
	//They're used to create complex data that contains pieces of information that are relevant to each other
	//Every individual piece of information is called a property of teh object

	//Syntax
	/*		let/const objectName = {
				propertyA:value,
				propertyB: "value", value
		}
	*/

	let person = {
		fullName: 'Edward Scissorhands',
		age: 25,
		isMarried: false,
		contact: ["+63917234567", "8123 4567"],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(person);

	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading:94.6
	}
	console.log(myGrades);

	//typeof operator

	console.log(typeof myGrades); //object
	console.log(typeof grades); //object
	//note: array is a special type of objects with methods and functions to manipulate it

	const anime = ['OP', 'OPM', 'BNHA']
	//re-aasignment of index 0
	// anime [0] = ['JJK'] //array within an array
	anime [0] = 'JJK'; //re-assignment
	console.log(anime);

	//Null

	//Null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
	let spouse = null;
	console.log(spouse)
	//null is considered a data type of its own compared to a zero which is a number and single quotes which are a data type of a string

	let myNumber = 0; //0
	let myString = ''; //wala nakalagay
	console.log(myNumber);
	console.log(myString);

	//Undefined
	//reperesent the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName); //undefined

	//one clear difference between undefined and null is that for undefined a variable was declared but was not provided a value 



